package com.tvshows.mvvm_tvshows.Network;

import com.tvshows.mvvm_tvshows.Model.TvShowResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("most-popular")
    Call<TvShowResponse> getmostpopulartvshows(@Query("page") int page);
}
