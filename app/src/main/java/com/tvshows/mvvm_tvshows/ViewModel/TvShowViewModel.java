package com.tvshows.mvvm_tvshows.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.tvshows.mvvm_tvshows.Model.TvShowResponse;
import com.tvshows.mvvm_tvshows.reposority.MostPopularTvShowsreposotry;

public class TvShowViewModel extends ViewModel {

    private   MostPopularTvShowsreposotry mostPopularTvShowsreposotry;

    public TvShowViewModel (){
        mostPopularTvShowsreposotry = new MostPopularTvShowsreposotry();
    }

    public LiveData<TvShowResponse> getmostpolulartvshowre(int page){
        return mostPopularTvShowsreposotry.getPopulartvshows(page);
    }
}
