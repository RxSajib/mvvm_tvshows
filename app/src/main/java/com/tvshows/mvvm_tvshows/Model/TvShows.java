package com.tvshows.mvvm_tvshows.Model;

import com.google.gson.annotations.SerializedName;

public class TvShows {

    @SerializedName("id")
    private String id;


    @SerializedName("name")
    private String name;

    @SerializedName("start_date")
    private String startdate;

    @SerializedName("country")
    private String country;

    @SerializedName("network")
    private String network;

    @SerializedName("status")
    private String status;

    @SerializedName("image_thumbnail_path")
    private String imagetham;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStartdate() {
        return startdate;
    }

    public String getCountry() {
        return country;
    }

    public String getNetwork() {
        return network;
    }

    public String getStatus() {
        return status;
    }

    public String getImagetham() {
        return imagetham;
    }
}
