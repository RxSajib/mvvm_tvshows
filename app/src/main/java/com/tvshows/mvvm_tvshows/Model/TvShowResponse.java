package com.tvshows.mvvm_tvshows.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowResponse {

    @SerializedName("page")
    private int page;


    @SerializedName("pages")
    private int totalpages;

    @SerializedName("tv_shows")
    private List<TvShows> tvShows;


    public int getPage() {
        return page;
    }

    public int getTotalpages() {
        return totalpages;
    }

    public List<TvShows> getTvShows() {
        return tvShows;
    }
}
