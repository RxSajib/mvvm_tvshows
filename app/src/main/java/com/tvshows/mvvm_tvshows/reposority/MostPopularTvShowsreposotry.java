package com.tvshows.mvvm_tvshows.reposority;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.tvshows.mvvm_tvshows.Model.TvShowResponse;
import com.tvshows.mvvm_tvshows.Network.ApiClint;
import com.tvshows.mvvm_tvshows.Network.ApiServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MostPopularTvShowsreposotry {

    private ApiServices apiServices;

    public MostPopularTvShowsreposotry(){
        apiServices = ApiClint.getRetrofit().create(ApiServices.class);
    }



    public LiveData<TvShowResponse> getPopulartvshows(int page){
        MutableLiveData<TvShowResponse> data = new MutableLiveData<>();

        apiServices.getmostpopulartvshows(page).enqueue(new Callback<TvShowResponse>() {
            @Override
            public void onResponse(@Nullable Call<TvShowResponse> call, @Nullable Response<TvShowResponse> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@Nullable Call<TvShowResponse> call, @Nullable Throwable t) {
                data.setValue(null);
            }
        });

        return data;

    };
}
