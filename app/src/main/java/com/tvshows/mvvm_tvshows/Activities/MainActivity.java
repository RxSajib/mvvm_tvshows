package com.tvshows.mvvm_tvshows.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.tvshows.mvvm_tvshows.Model.TvShowResponse;
import com.tvshows.mvvm_tvshows.R;
import com.tvshows.mvvm_tvshows.ViewModel.TvShowViewModel;

public class MainActivity extends AppCompatActivity {

    private TvShowViewModel tvShowViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tvShowViewModel = new ViewModelProvider(this).get(TvShowViewModel.class);

        getmostpolulartvshows();
    }

    private void getmostpolulartvshows(){
        tvShowViewModel.getmostpolulartvshowre(1).observe(this, new Observer<TvShowResponse>() {
            @Override
            public void onChanged(TvShowResponse tvShowResponse) {
                Toast.makeText(MainActivity.this, String.valueOf(tvShowResponse.getTotalpages()), Toast.LENGTH_SHORT).show();
            }
        });
    }
}